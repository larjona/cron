#!/bin/sh -e

# This file updates the doc/ directory of the web pages

# Oh, how I wish the -doc people would maintain this. -- Jay Treacy
# There ain't no such thing as the -doc people these days :/ -- Josip Rodin
# Let me refactor and add some recent docs: osamu

# ftpdir/doc is populated by 1ftpfiles
# ftpdir/pool is populated by 1ftpfiles with timestamped *.deb files

. `dirname $0`/../common.sh
webdocdir=$webdir/doc
ftpdocdir=$ftpdir/doc

#############################################################################
mkdirp()
{
test -d $1 || install -d -m 2775 $1
}

#############################################################################
# unpack all debs to cleaned $crondir/tmp
rm -rf $crondir/tmp
mkdirp $crondir/tmp
cd $crondir/tmp

#############################################################################
unpack()
{
namebin=$1     # binary package name: maint-guide-fr
echo -n " ${namebin}"
filedeb=`ls -t1 $ftpdir/pool/${namebin}_*.deb | head -1`
dpkg-deb -x $filedeb .
}

#############################################################################
mvdocs()
{
namedest=$1    # destdir directory:  maint-guide
basedir=$2     # binary package data dir.: usr/share/doc/maint-guide-fr
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en)
 # YES: with    $lang and leave it so
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-$namedest}  # base filename w/o .txt or $lang.txt

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

for ext in epub pdf txt text ps; do
	sourcepath=$basedir/${namedoc}${lang0}.$ext
	if [ -f "`readlink -f $sourcepath.gz`" ]; then
		rm -f $sourcepath
		zcat `readlink -f $sourcepath.gz` >$sourcepath
	elif [ -f "`readlink -f $sourcepath.xz`" ]; then
		rm -f $sourcepath
		xzcat `readlink -f $sourcepath.xz` >$sourcepath
	fi
	if [ "$ext" = "text" ]; then
		ext="txt"
	fi
	if [ "${addlang}" = "NO" ]; then
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$ext
		fi
	elif [ "${addlang}" = "ADD" ]; then
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$lang.$ext
			if [ "$lang" = "en" ]; then
				ln -sf ${namedoc}.en.$ext $destdir/${namedoc}.$ext
			fi
		fi
	else # YES
		if [ -f "`readlink -f $sourcepath`" ]; then
			install -p -m 664 `readlink -f $sourcepath` $destdir/${namedoc}.$lang.$ext
		fi
	fi
done
}

#############################################################################
pagecopy()
{
# convert all internal href URLs from *.html to *.$lang.html
lang=$1
infile=$2
outfile=$3
sed -e "s/\(href=\"[^\/:\"= ]*\)\.html/\1.$lang.html/g" <$infile >$outfile
}

#############################################################################
mvhtml()
{
namedest=$1    # destdir directory:  maint-guide
basedir=$2     # binary package data dir.: usr/share/doc/maint-guide-fr/html
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

pagepattern="$basedir/${namedoc}${lang0}.html"
for page in $pagepattern; do
	pagefile="`readlink -f $page`"
	if [ -f "$pagefile" ]; then
		if [ "${addlang}" = "ADD" ]; then
			if [ "$(basename $page $lang.html)" = "$(basename $page)" ]; then
				# This is not *.$lang.html file but *.html
				pagecopy $lang "$pagefile" "$destdir/$(basename $page .html).$lang.html"
				if [ "$lang" = "en" ]; then
					ln -sf "$(basename $page .html).en.html" "$destdir/$(basename $page)"
				fi
			fi
		elif [ "${addlang}" = "YES" ]; then
			# This is *.$lang.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
			if [ "$lang" = "en" ]; then
				ln -sf "$(basename $page)" "$destdir/$(basename $page .en.html).html"
			fi
		else # NO This is *.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	fi
done

if [ "$lang" = "en" ]; then
	pagepattern="$basedir/*.css"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	done
	pagepattern="$basedir/images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/images
			install -p -m 664 `readlink -f $page` $destdir/images/$(basename $page)
		fi
	done
fi
}

#############################################################################
pagecopy2()
{
# convert all internal href URLs from *.html to *.$lang.html
# move language dependent images
lang=$1
infile=$2
outfile=$3
sed -e "s/\(href=\"[^\/:\"= ]*\)\.html/\1.$lang.html/g" \
    -e "s/\(<img src=\"images\)\//\1.$lang\//g" \
		<$infile >$outfile
}

#############################################################################
mvhtml2()
{
namedest=$1    # destdir directory: debian-handbook 
basedir=$2     # binary package data dir.: usr/share/doc/debian-handbook/html/$lang 
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

pagepattern="$basedir/${namedoc}${lang0}.html"
for page in $pagepattern; do
	pagefile="`readlink -f $page`"
	if [ -f "$pagefile" ]; then
		if [ "${addlang}" = "ADD" ]; then
			if [ "$(basename $page $lang.html)" = "$(basename $page)" ]; then
				# This is not *.$lang.html file but *.html
				pagecopy2 $lang "$pagefile" "$destdir/$(basename $page .html).$lang.html"
				if [ "$lang" = "en" ]; then
					ln -sf "$(basename $page .html).en.html" "$destdir/$(basename $page)"
				fi
			fi
		elif [ "${addlang}" = "YES" ]; then
			# This is *.$lang.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
			if [ "$lang" = "en" ]; then
				ln -sf "$(basename $page)" "$destdir/$(basename $page .en.html).html"
			fi
		else # NO This is *.html file
			install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
		fi
	fi
done

# language dependent images
pagepattern="$basedir/images/*"
for page in $pagepattern ; do
	if [ -f "`readlink -f $page`" ]; then
		mkdirp $destdir/images.$lang
		install -p -m 664 `readlink -f $page` $destdir/images.$lang/$(basename $page)
	fi
done
if [ "$lang" = "en" ]; then
	pagepattern="$basedir/Common_Content/css/*.css"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/Common_Content/css
			install -p -m 664 `readlink -f $page` $destdir/Common_Content/css/$(basename $page)
		fi
	done
	pagepattern="$basedir/Common_Content/images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/Common_Content/images
			install -p -m 664 `readlink -f $page` $destdir/Common_Content/images/$(basename $page)
		fi
	done
fi
}

#############################################################################

mvhtml_sphinx()
{
# Copy of mvhtml(), simplified because we just copy the 1-page-manual and 
# the _images and _static subfolders
# This is needed by debian-policy since they moved to reStructuredText and Sphinx
# This is probably uncomplete, since the _static folder contains symlinks to 
# some javascript that probably will not work. 

namedest=$1    # destdir directory:  debian-policy
basedir=$2     # binary package data dir.: usr/share/doc/debian-policy-fr/html
addlang=${3:-NO} # $lang in filename: NO | ADD | YES
 # NO:  without $lang and leave it so
 # ADD: without $lang and add it (make link for en) (internal URL conversion)
 # YES: with    $lang and leave it so (make link for en)
lang=${4:-en}  # language name: en (default), fr, ...
sectdir=${5:-manuals/}  # destination top directory
                        # set to / or packaging-manuals/ if not
namedoc=${6:-*}         # filemask w/o .html or .$lang.html

destdir=$webdocdir/${sectdir}$namedest
mkdirp $destdir

if [ "${addlang}" = "YES" ]; then
	lang0=".$lang"
else # NO ADD
	lang0=""
fi

# copying only the 1-page-manual
# not taking care of translations; when they are there, we'll see how to copy them
pagepattern="$basedir"
for page in $pagepattern; do
    pagefile="`readlink -f $page`"
    if [ -f "$pagefile" ]; then
     install -p -m 664 `readlink -f $page` $destdir/$(basename $page)
    fi
done

if [ "$lang" = "en" ]; then
	pagepattern="$basedir/_static/*"
	for page in $pagepattern; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/_static
			install -p -m 664 `readlink -f $page` $destdir/_static/$(basename $page)
		fi
	done
	pagepattern="$basedir/_images/*"
	for page in $pagepattern ; do
		if [ -f "`readlink -f $page`" ]; then
			mkdirp $destdir/_images
			install -p -m 664 `readlink -f $page` $destdir/_images/$(basename $page)
		fi
	done
fi
}

#############################################################################
lclocal()
{
case $1 in
zh_CN | zh-CN) echo "zh-cn" ;;
zh_TW | zh-TW) echo "zh-tw" ;;
pt_BR | pt-BR) echo "pt-br" ;;
*-*) echo "${1%%-*}" ;;
*_*) echo "${1%%_*}" ;;
*) echo "${1}" ;;
esac
}

#############################################################################
uplocal()
{
case $1 in
zh-cn) echo "zh_CN" ;;
zh-tw) echo "zh_TW" ;;
pt-br) echo "pt_BR" ;;
*) echo "$1" ;;
esac
}

#############################################################################
pkg2lang()
{
mask=$1 # aptitude-doc- w/o *
cd  $ftpdir/pool >/dev/null
ls -1 $mask*.deb | \
sed -e "s/^$mask//g" -e 's/_.*\.deb$//g' | \
sort | uniq
cd - >/dev/null
}

#############################################################################
file2lang()
{
mask=$1 # debmake-doc w/o *.txt.gz
ext=${2:-.txt.gz}
if [ -z "$3" ]; then
directory="usr/share/doc/${mask}"
else
directory="$3"
fi
cd  $crondir/tmp/$directory >/dev/null
ls -1 ${mask}.*${ext} | \
sed -e "s/^${mask}\.//g" -e "s/$ext\$//g"
cd - >/dev/null
}

#############################################################################
dir2lang()
{
directory="$1"
cd  $crondir/tmp/$directory >/dev/null
ls -1d * ; \
cd - >/dev/null
}

#############################################################################
echo -n "Installing documents:"

# Debian Policy Manual
unpack debian-policy
mvdocs debian-policy usr/share/doc/debian-policy NO en / policy
mvhtml_sphinx debian-policy usr/share/doc/debian-policy/policy-1.html NO en /
mv -f $webdocdir/debian-policy/policy-1.html $webdocdir/debian-policy/index.html

mvdocs fhs usr/share/doc/debian-policy/fhs NO en packaging-manuals/ fhs-2.3
mvhtml fhs usr/share/doc/debian-policy/fhs NO en packaging-manuals/ fhs-2.3

mvdocs menu-policy usr/share/doc/debian-policy NO en packaging-manuals/
mvhtml menu-policy usr/share/doc/debian-policy/menu-policy.html NO en packaging-manuals/

mvdocs perl-policy usr/share/doc/debian-policy NO en packaging-manuals/
mvhtml perl-policy usr/share/doc/debian-policy/perl-policy.html NO en packaging-manuals/

mvhtml packaging-manuals usr/share/doc/debian-policy NO en / debconf_specification
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / libc6-migration
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / upgrading-checklist
mvdocs packaging-manuals usr/share/doc/debian-policy NO en / virtual-package-names-list

mvdocs packaging-manuals usr/share/doc/debian-policy NO en / copyright-format-1.0
mvhtml packaging-manuals usr/share/doc/debian-policy NO en / copyright-format-1.0
# Move the copyright-format files to its canonical place
mkdirp $webdocdir/packaging-manuals/copyright-format/1.0
mv -f $webdocdir/packaging-manuals/copyright-format-1.0.html $webdocdir/packaging-manuals/copyright-format/1.0/index.html
mv -f $webdocdir/packaging-manuals/copyright-format-1.0.txt  $webdocdir/packaging-manuals/copyright-format/1.0/copyright-format.txt

# Other policy documents
unpack build-essential
install -p -m 664 usr/share/doc/build-essential/list $webdocdir/packaging-manuals/build-essential

unpack menu
mvhtml menu.html usr/share/doc/menu/html NO en packaging-manuals/

unpack emacsen-common
gunzip usr/share/doc/emacsen-common/debian-emacs-policy.gz
install -p -m 664 usr/share/doc/emacsen-common/debian-emacs-policy $webdocdir/packaging-manuals

unpack java-policy
rm -rf $webdocdir/packaging-manuals/java-policy
mkdir $webdocdir/packaging-manuals/java-policy
mvhtml java-policy     usr/share/doc/java-policy/debian-java-policy NO en packaging-manuals/
mvhtml debian-java-faq usr/share/doc/java-policy/debian-java-faq NO en

unpack dpkg-doc
mvhtml dpkg-internals  usr/share/doc/dpkg/internals NO en packaging-manuals/

unpack python
mvhtml python-policy   usr/share/doc/python/python-policy.html NO en packaging-manuals/

# Other manuals
unpack debian-refcard
# Too irregular to handle with mvdocs
for refcardfile in usr/share/doc/debian-refcard/*a4.pdf.gz ; do
	# rename from refcard-fr-a4.pdf.gz to refcard.fr.pdf as expected by the website
	futurename=`echo $refcardfile | sed 's/refcard-\(\w*\)-a4.pdf.gz/refcard.\1.pdf/'`
	gunzip -f $refcardfile && mv usr/share/doc/debian-refcard/$(basename $refcardfile .gz) $futurename
done
mkdirp $webdocdir/manuals/refcard
install -p -m 664 usr/share/doc/debian-refcard/*pdf $webdocdir/manuals/refcard/

unpack packaging-tutorial
mvdocs packaging-tutorial usr/share/doc/packaging-tutorial ADD en
langlist=`file2lang packaging-tutorial .pdf`
for lang in $langlist ; do
	mvdocs packaging-tutorial usr/share/doc/packaging-tutorial YES $lang
done

unpack developers-reference
mvdocs developers-reference usr/share/doc/developers-reference ADD en
mvhtml developers-reference usr/share/doc/developers-reference ADD en
langlist=`pkg2lang developers-reference-`
for lang in $langlist ; do
	unpack developers-reference-$lang
	mvdocs developers-reference usr/share/doc/developers-reference-$lang ADD $lang
	mvhtml developers-reference usr/share/doc/developers-reference-$lang ADD $lang
done

unpack debian-faq
mvdocs debian-faq usr/share/doc/debian/FAQ YES en
mvhtml debian-faq usr/share/doc/debian/FAQ YES en
langlist=`pkg2lang debian-faq-`
for lang in $langlist ; do
	unpack debian-faq-$lang
	mvdocs debian-faq usr/share/doc/debian/FAQ YES `uplocal $lang`
	mvhtml debian-faq usr/share/doc/debian/FAQ/`uplocal $lang` YES $lang
done

unpack maint-guide
mvdocs maint-guide usr/share/doc/maint-guide YES en
mvhtml maint-guide usr/share/doc/maint-guide/html YES en
langlist=`pkg2lang maint-guide-`
# ko pl pt-br zh-cn zh-tw are old (zh-tw and zh-tw is in NEW)
# once NEW pass
langlist=`echo $langlist | sed -e s/ko// -e s/pl// -e s/pt-br//`
for lang in $langlist ; do
	unpack maint-guide-$lang
	mvdocs maint-guide usr/share/doc/maint-guide-$lang YES $lang
	mvhtml maint-guide usr/share/doc/maint-guide-$lang/html YES $lang
done

unpack debian-reference-common
langlist=`pkg2lang debian-reference-`
for lang in $langlist ; do
	unpack debian-reference-$lang
	mvdocs debian-reference usr/share/debian-reference YES $lang 
	mvhtml debian-reference usr/share/debian-reference YES $lang
done

unpack debmake-doc
langlist=`file2lang debmake-doc`
for lang in $langlist ; do
	mvdocs debmake-doc usr/share/doc/debmake-doc YES $lang
	mvhtml debmake-doc usr/share/doc/debmake-doc/html YES $lang
done

unpack apt-doc
mvdocs apt-guide usr/share/doc/apt-doc YES en manuals/ guide
mvhtml apt-guide usr/share/doc/apt-doc/guide.html ADD en
langlist=`file2lang guide .text.gz usr/share/doc/apt-doc`
for lang in $langlist ; do
	mvdocs apt-guide usr/share/doc/apt-doc YES $lang manuals/ guide
	ln -sf guide.$lang.txt $webdocdir/manuals/apt-guide/apt-guide.$lang.txt
	mvhtml apt-guide usr/share/doc/apt-doc/guide.$lang.html ADD $lang
done
mvdocs apt-offline usr/share/doc/apt-doc YES en manuals/ offline
mvhtml apt-offline usr/share/doc/apt-doc/offline.html ADD en 
langlist=`file2lang offline .text.gz usr/share/doc/apt-doc`
for lang in $langlist ; do
	mvdocs apt-offline usr/share/doc/apt-doc YES $lang manuals/ offline
	ln -sf offline.$lang.txt $webdocdir/manuals/apt-offline/apt-offline.$lang.txt
	mvhtml apt-offline usr/share/doc/apt-doc/offline.$lang.html ADD $lang
done

langlist=`pkg2lang aptitude-doc-`
for lang in $langlist ; do
	unpack aptitude-doc-$lang
	mvhtml aptitude usr/share/doc/aptitude/html/$lang ADD $lang
done

unpack debian-kernel-handbook
mvhtml debian-kernel-handbook usr/share/doc/debian-kernel-handbook/kernel-handbook.html NO en

unpack debian-handbook
langlist=`dir2lang usr/share/doc/debian-handbook/html`
for lang in $langlist ; do
	lclang=`lclocal $lang`
	mvhtml2 debian-handbook usr/share/doc/debian-handbook/html/$lang ADD $lclang
done

unpack dbconfig-common
mvdocs dbapp-policy usr/share/doc/dbconfig-common NO en manuals/ dbapp-policy
mvdocs dbconfig-common usr/share/doc/dbconfig-common NO en manuals/ dbconfig-common
mvhtml dbapp-policy usr/share/doc/dbconfig-common/dbapp-policy.html NO en
mvhtml dbconfig-common usr/share/doc/dbconfig-common NO en
mvhtml dbconfig-common usr/share/doc/dbconfig-common/dbconfig-common.html NO en

echo
echo "7doc finished (at `date`)"

echo


